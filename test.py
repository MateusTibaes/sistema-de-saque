def saque(valor_saque):
  """
  Simula o saque de dinheiro, informando quais notas serão entregues.

  Args:
      valor_saque (int): Valor que o usuário deseja sacar.

  Returns:
      dicionario: Dicionário contendo a quantidade de cada cédula a ser entregue.
  """
  # Notas disponíveis e suas quantidades iniciais
  notas = {
      200: 0,
      100: 0,
      50: 0,
      20: 0,
      10: 0,
      5: 0,
  }

  # Entregando as maiores cédulas possíveis
  for nota in notas.keys():
    while valor_saque >= nota:
      valor_saque -= nota
      notas[nota] += 1

  # Retornando o dicionário com as notas e suas quantidades
  return notas

# Valor que o usuário deseja sacar
valor_saque = int(input("Digite o valor que deseja sacar: "))

# Obtendo as notas e quantidades a serem entregues
notas_a_entregar = saque(valor_saque)

# Mostrando o resultado
print("\n**Notas a serem entregues:**")
for nota, quantidade in notas_a_entregar.items():
  if quantidade > 0:
    print(f"- {quantidade} notas de R${nota}")

# Informando o saldo restante (se implementado)
# saldo_restante = ...
# print(f"\nSaldo restante: R${saldo_restante}")
